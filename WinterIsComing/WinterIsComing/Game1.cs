﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace WinterIsComing
{
    //    this game is a first-try game of 3d world.
    //    in this game we get an observe from the sky down to the "people" (robot) 
    //    we have the ability to throw winter things on them such as rain, snow, lightnings and more.
    //    hopefuly i'll managed importing this models in the future
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        Camera camera;
        InputHandler input;

        RasterizerState wireframeRaster, solidRaster;
        bool drawWireframe;

        TearDrop tearDrops;
        Robot robot;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // screen size of the game
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 800;
            // input obj to control arrows and space keys.
            input = new InputHandler(this);
            Components.Add(input);
            //camera with an upper view
            camera = new PerspectiveCamera(this);
            camera.CameraPosition = new Vector3(0.0f, 5.0f, 3.0f);
            Components.Add(camera);
            //tear drop model i tried to build for the "rain"
            tearDrops = new TearDrop(this, input);
            Components.Add(tearDrops);
            //robot model as what was shown in class with minor changes.
            robot = new Robot(this);
            Components.Add(robot);
            //modes for screen
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;

            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;

            drawWireframe = false;
        }

       
        protected override void Initialize()
        {
            base.Initialize();
        }

      
        protected override void LoadContent()
        {
            base.LoadContent();
        }

     
        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (input.KeyboardHandler.WasKeyPressed(Keys.W))
                drawWireframe = !drawWireframe;
            base.Update(gameTime);

        }

        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.Black);

            if (drawWireframe)
                GraphicsDevice.RasterizerState = wireframeRaster;
            else
                GraphicsDevice.RasterizerState = solidRaster;

            Matrix world = Matrix.Identity;

            world =
                Matrix.CreateScale(0.25xf) * // Scale everything to a smaller size
                world;

            tearDrops.Camera = camera;
            tearDrops.World = world;

            robot.Camera = camera;
            robot.World = world;

            base.Draw(gameTime);

        }
    }
}

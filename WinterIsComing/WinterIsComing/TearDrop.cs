﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WinterIsComing
{
    // this is a model class that contains 5 teardrops and serves the game as "rain"
    public class TearDrop : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private Model tears;
        private BasicEffect effect;

        public Camera Camera { get; set; }
        public Matrix World { get; set; }
        private Matrix Movement { get; set; }
        InputHandler input;

        private float bodyRotationRate = 0.0f;
        private float down = 0.0f;
        private bool end = false;
        private float movementLocX = 0.1f;
        private float movementLocZ = 0.1f;

        public TearDrop(Game game, InputHandler i) : base(game)
        {
            input = i; 
            Movement = Matrix.CreateTranslation(new Vector3(0.0f, 0.0f, 0.0f));
        }

        protected override void LoadContent()
        {
            tears = Game.Content.Load<Model>(@"Meshes\TearDrop");

            effect = new BasicEffect(Game.GraphicsDevice);
            effect.VertexColorEnabled = true;
        }

        protected override void UnloadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            // if the player pressed "space" then it is the end of this component life and it is supposed to be dropped down.
            if (end)
            {
                down -= 1.0f;
                Movement = Matrix.CreateTranslation(new Vector3( movementLocX, down, movementLocZ));
            }
            // as long as "space" wasn't pressed ,the player can move the drops with the arrows in order to be exactky above the target.
            else
            {
                if (input.KeyboardHandler.IsKeyDown(Keys.Right))
                {
                    movementLocX += 0.1f;
                    movementLocZ -= 0.1f;
                    Movement = Matrix.CreateTranslation(new Vector3(movementLocX, 0.0f,  movementLocZ));
                }
                if (input.KeyboardHandler.IsKeyDown(Keys.Left))
                {
                    movementLocX -= 0.1f;
                    movementLocZ += 0.1f;
                    Movement = Matrix.CreateTranslation(new Vector3(movementLocX, 0.0f,  movementLocZ));
                }
                if (input.KeyboardHandler.IsKeyDown(Keys.Down))
                {
                    movementLocX += 0.1f;
                    movementLocZ += 0.1f;
                    Movement = Matrix.CreateTranslation(new Vector3(movementLocX, 0.0f, movementLocZ));
                }
                if (input.KeyboardHandler.IsKeyDown(Keys.Up))
                {
                    movementLocX -= 0.1f;
                    movementLocZ -= 0.1f;
                    Movement = Matrix.CreateTranslation(new Vector3(movementLocX, 0.0f, movementLocZ));
                }
                if (input.KeyboardHandler.WasKeyPressed(Keys.Space))
                {
                    end = true;
                }

                bodyRotationRate += 0.05f;
            }

            base.Update(gameTime);
        }


       
        public override void Draw(GameTime gameTime)
        {
            // creating and drawing the tears on the 3d world with ability of rotating in place each
            Matrix tear1 =
                Matrix.CreateScale(new Vector3(0.5f, 1.0f, 0.5f)) * Matrix.CreateRotationY(bodyRotationRate ) *
                Matrix.CreateTranslation(new Vector3(0.0f, 7.25f, 0.0f))*Movement;

            DrawTears(gameTime, tear1);

            Matrix tear2 =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) * Matrix.CreateRotationY(bodyRotationRate + 0.1f) *
                Matrix.CreateTranslation(new Vector3(1.0f, 6.25f, 0.0f)) * Movement;

            DrawTears(gameTime, tear2);

            Matrix tear3 =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) * Matrix.CreateRotationY(bodyRotationRate + 0.3f) *
                Matrix.CreateTranslation(new Vector3(-1.0f, 6.25f, 0.0f)) * Movement;

            DrawTears(gameTime, tear3);

            Matrix tear4 =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) * Matrix.CreateRotationY(bodyRotationRate + 0.5f) *
                Matrix.CreateTranslation(new Vector3(0.375f, 5.5f, 0.0f)) * Movement;

            DrawTears(gameTime, tear4);

            Matrix tear5 =
                Matrix.CreateScale(new Vector3(0.5f, 1.5f, 0.5f)) * Matrix.CreateRotationY(bodyRotationRate + 0.7f) *
                Matrix.CreateTranslation(new Vector3(-0.375f, 4.0f, 0.0f)) * Movement;

            DrawTears(gameTime, tear5);

            // creating and drawing a "guiding line" that should help the player see when he is above the target.
            for (int i = 0; i < 5; i++)
            {
                Matrix line=
                              Matrix.CreateScale(new Vector3(0.05f, 0.4f, 0.05f)) *
                              Matrix.CreateTranslation(new Vector3(-0.375f, 4.0f-i, 0.0f)) * Movement;

                DrawTears(gameTime, line);
            }
          

            base.Draw(gameTime);
        }

        private void DrawTears(GameTime gameTime, Matrix CubeTransform)
        {
            foreach (ModelMesh mesh in tears.Meshes)
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    GraphicsDevice.SetVertexBuffer(meshPart.VertexBuffer, meshPart.VertexOffset);
                    GraphicsDevice.Indices = meshPart.IndexBuffer;

                    effect.World = CubeTransform * World;
                    effect.View = Camera.View;
                    effect.Projection = Camera.Projection;
                    effect.CurrentTechnique.Passes[0].Apply();

                    GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, meshPart.PrimitiveCount);
                }
            }
        }

        

    }
}

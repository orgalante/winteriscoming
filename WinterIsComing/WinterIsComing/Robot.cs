using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XELibrary;

namespace WinterIsComing
{
    public class Robot : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private Model cube;
        private BasicEffect effect;

        public Camera Camera { get; set; }
        public Matrix World { get; set; }
        private Matrix direction { get; set; }
        private Matrix movement { get; set; }
        public Matrix headTransform { get; set; }
        private bool back = false;

        private float handWaveRate = -20.0f;
        private float handWaveAngle = 0.0f;
        private float movementLoc = 0.1f;

        public Robot(Game game)
            : base(game)
        {

        }

        

        protected override void LoadContent()
        {
            cube = Game.Content.Load<Model>(@"Meshes\Cube");
            effect = new BasicEffect(Game.GraphicsDevice);
            effect.VertexColorEnabled = true;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            direction = Matrix.CreateRotationY(5.5f);
            

            handWaveAngle += handWaveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (System.Math.Abs(handWaveAngle) > 40.0f)
            {
                handWaveRate = -handWaveRate;
                handWaveAngle = 40.0f * System.Math.Sign(handWaveAngle);
            }


            if (!back)
            {
                movementLoc += 0.01f;
                if (movementLoc > 5)
                    back = true;
            }
            else
            {
                movementLoc -= 0.01f;
                if (movementLoc <-5)
                    back = false;
            }
            

            movement = Matrix.CreateTranslation(new Vector3(-1*movementLoc, 0.0f, movementLoc));

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {



            // Head
             headTransform =
                Matrix.CreateTranslation(new Vector3(0.0f, 1.25f, 0.0f)) * direction*movement;
        

            DrawCube(gameTime, headTransform);


            // Body
            Matrix bodyTransform =
                Matrix.CreateScale(new Vector3(1.5f, 1.5f, 1.5f)) * direction * movement;


            DrawCube(gameTime, bodyTransform);

            // Left Arm
            Matrix leftArmTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *

                   Matrix.CreateTranslation(new Vector3(0.0f, -0.75f, 0.0f)) *
                Matrix.CreateRotationX(MathHelper.ToRadians(handWaveAngle * (-1))) *
                Matrix.CreateTranslation(new Vector3(0.0f, 0.75f, 0.0f)) *

                Matrix.CreateTranslation(new Vector3(1.0f, -0.25f, 0.0f)) * direction * movement;

            DrawCube(gameTime, leftArmTransform);

            // Right Arm
            Matrix rightArmTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *

                Matrix.CreateTranslation(new Vector3(0.0f, -0.75f, 0.0f)) *
                Matrix.CreateRotationX(MathHelper.ToRadians(handWaveAngle)) *
                Matrix.CreateTranslation(new Vector3(0.0f, 0.75f, 0.0f)) *

                Matrix.CreateTranslation(new Vector3(-1.0f, -0.25f, 0.0f)) * direction * movement;

            DrawCube(gameTime, rightArmTransform);

            // Left Leg
            Matrix leftLegTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *

                Matrix.CreateTranslation(new Vector3(0.0f, -0.75f, 0.0f)) *
                Matrix.CreateRotationX(MathHelper.ToRadians(handWaveAngle)) *
                Matrix.CreateTranslation(new Vector3(0.0f, 0.75f, 0.0f)) *

                Matrix.CreateTranslation(new Vector3(0.375f, -1.75f, 0.0f)) * direction * movement;

            DrawCube(gameTime, leftLegTransform);

            // Right Leg
            Matrix rightLegTransform =
                Matrix.CreateScale(new Vector3(0.5f, 2.0f, 0.5f)) *

                Matrix.CreateTranslation(new Vector3(0.0f, -0.75f, 0.0f)) *
                Matrix.CreateRotationX(MathHelper.ToRadians(handWaveAngle * (-1))) *
                Matrix.CreateTranslation(new Vector3(0.0f, 0.75f, 0.0f)) *

                Matrix.CreateTranslation(new Vector3(-0.375f, -1.75f, 0.0f)) * direction * movement;

            DrawCube(gameTime, rightLegTransform);

            base.Draw(gameTime);
        }


        private void DrawCube(GameTime gameTime, Matrix CubeTransform)
        {
            foreach (ModelMesh mesh in cube.Meshes)
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    GraphicsDevice.SetVertexBuffer(meshPart.VertexBuffer, meshPart.VertexOffset);
                    GraphicsDevice.Indices = meshPart.IndexBuffer;

                    effect.World = CubeTransform * World;
                    effect.View = Camera.View;
                    effect.Projection = Camera.Projection;
                    effect.CurrentTechnique.Passes[0].Apply();

                    GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, meshPart.PrimitiveCount);
                }
            }
        }
    }
}
